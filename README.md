# Open Source Software

A list of my OSS contributions:

- [GitHub Pull Requests](https://github.com/pulls?q=is:pr+author:TomasHubelbauer)
- [GitLab Merge Requests](https://gitlab.com/dashboard/merge_requests?author_id=375110&state=all)

- [ ] Fill the picks here in a nicer form

## Highlights

- My suggestion to VS Code to disable invoking `push` when syncing with Git `origin` in case the remote has it's `push` URL set to `no_push`
- My suggestion to VS Code to display multiple sync buttons for Git if there is no `origin` remote (presumably because a team treats multiple remotes as an origin)
